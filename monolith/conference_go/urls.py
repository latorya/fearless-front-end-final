from django.contrib import admin  # type: ignore
from django.urls import path, include  # type: ignore

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/", include("events.api_urls")),
    path("api/", include("presentations.api_urls")),
    path("api/", include("accounts.api_urls")),
]
