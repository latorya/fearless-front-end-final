window.addEventListener('DOMContentLoaded', async () => {
    // reference url = location list
    const url = 'http://localhost:8000/api/locations/';
    // reference response from url
    const response = await fetch(url);

    // create formTag for create-conference-form
    const formTag = document.getElementById('create-conference-form');
    // add event listener for type submit
    formTag.addEventListener('submit', async event => {
        // prevent default
        event.preventDefault();
        // create data object and stringify
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        // create reference url for conference list
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        // configure fetch
        const fetchConfig= {
            method: "post",
            body: json,
            header: {
                'Content-Type': 'application/json',
            },
        };
        // new response from conferenceUrl and fetchConfig
        const conferenceresponse = await fetch(conferenceUrl, fetchConfig);
        // if response.ok do something with it
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
        }
    });

    // if response.ok
    if (response.ok) {
        const locationData = await response.json();
        const selectTag = document.getElementById('location');
        for (let location of locationData.locations) {
            const option = document.createElement('option');

            // set vale = location id and innerhtml as name
            option.value = location.id;
            option.innerHTML = location.name;
            selectTag.append(option);
        }
    }
});
