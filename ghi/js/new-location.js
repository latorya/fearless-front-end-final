window.addEventListener('DOMContentLoaded', async () => {
    // create url variable
    const stateslistURL = 'http://localhost:8000/api/states/';
    // create a response variable
    const stateslistResponse = await fetch(stateslistURL);

    // create a formTag variable
    const formTag = document.getElementById('create-location-form');
    // add the event listener
    formTag.addEventListener('submit', async event => {
        // prevent default
        event.preventDefault();
        // create formData
        const formData = new FormData(formTag);
        // create json
        const json = JSON.stringify(Object.fromEntries(formData));
        // create url variable to reference list of locations
        const locationlistURL = 'http://localhost:8000/api/locations/';
        // define configurable fetch method
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        // declare response variable
        const response = await fetch(locationlistURL, fetchConfig);
        // if the response succeeds, then do something with it
        if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
            console.log(newLocation);
        }
    });

    // if response.ok then create data variable to do things to
    if (stateslistResponse.ok) {
        const statesData = await stateslistResponse.json();

        // create a tab variable
        const stateTag = document.getElementById('state');
        // iterate over state in states
        for (let state of statesData.states) {
            // create option variable
            const option = document.createElement('option');

            // set value = abbr and innerhtml = name
            option.value = state.abbreviation;
            option.innerHTML = state.name;

            // append option to selectTag
            stateTag.appendChild(option);
        }
    }
});
